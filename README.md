# UE LIFPF INF2030L Programmation Fonctionnelle

## Semestre 2023 Printemps

| jour  | heure         | type     | supports / remarques                                                      |
| ----- | ------------- | -------- | ------------------------------------------------------------------------- |
| 16/01 | 8h            | CM       | [Diapositives](cm/lifpf-cm1.pdf)                                          |
|       | 9h45          | TD       | [Sujet](td/lifpf-td1-enonce.pdf) <br> Groupes B et E à 11h30              |
| 23/01 | 8h            | CM       | [Diapositives](cm/lifpf-cm2.pdf), [Script démos](cm/cm2-demo.md)          |
|       | 9h45 ou 11h30 | TP       | [Sujet](tp/tp1.md) <br> Groupe de TP, horaire et salle sur [tomuss]       |
| 30/01 | 8h            | TD + QCM | [Sujet](td/lifpf-td2-enonce.pdf) <br> Groupes A et F en salle Nautibus C1 |
|       | 9h45 ou 11h30 | TP       | [Sujet](tp/tp2.md) <br> Groupe de TP, horaire et salle sur [tomuss]       |

[tomuss]: https://tomuss.univ-lyon1.fr
